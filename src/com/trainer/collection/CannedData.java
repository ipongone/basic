package com.trainer.collection;

import com.trainer.model.ExamHistory;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class CannedData {



  static String data1 =
      "Clara;Java;75\nClara;Dart;65.4\nClara;Python;88.5\nClara;C++;65\nClara;Javascript;70" +
      "\nTriss;Java;90\nTriss;Dart;80\nTriss;Python;95\nTriss;C++;75\nTriss;Javascript;80" +
      "\nCirilla;Java;95\nCirilla;Dart;85\nCirilla;Python;95\nCirilla;C++;85\nCirilla;Javascript;95";

  public static List<ExamHistory> codingExamList() {
    return Arrays.stream(data1.split("\n"))
        .map(x -> {
          String[] y = x.split(";");
          return new ExamHistory().name(y[0]).topic(y[1]).score(Double.parseDouble(y[2]));
        }).collect(Collectors.toList());
  }


  public static void main(String[] args) {

    // 1. Get list of string length < 5
    // 2. Make it unique collection
    List<String> list =
        Arrays.asList("Java", "Dart", "Python", "Java", "Dart", "Java", "C++", "Javascript",
            "Kotlin", "Dart", "Groovy", "Python");

    // 1. Get List of Name
    // 2. List of topic
    // 3. Get average score of Triss
    List<ExamHistory> histories = codingExamList();


  }
}
