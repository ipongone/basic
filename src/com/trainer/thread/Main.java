package com.trainer.thread;

public class Main {

  public static void main(String[] args) {
    //
    //1. Simulate 4 processes using anyMethod: e.g A, B, C, D synchronously
    //2. Simulate them Asynchronously
  }

  public static void anyMethod(String namaProcess, int procTime){
    System.out.println(String.format("==================> Start %s", namaProcess));
    try {
      Thread.sleep(procTime);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    System.out.println(String.format("======> Finished: %s", namaProcess));
  }

}
