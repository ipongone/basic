package com.trainer.oop.interfc;
import java.util.*;

// Create MyCalculator class that implements AdvancedAritMetic

// Output:
// I implemented: com.trainer.oop.interfc.AdvancedAritmetic
//Enter N: 20
//1 2 4 5 10 20
//Result: 42



public class Solution {
  public static void main(String []args){
    /*MyCalculator my_calculator = new MyCalculator();
    System.out.print("I implemented: ");
    ImplementedInterfaceNames(my_calculator);
    Scanner sc = new Scanner(System.in);
    System.out.print("Enter N: ");
    int n = sc.nextInt();
    System.out.println("\nResult: "+my_calculator.divisor_sum(n) + "\n");
    sc.close();*/
  }
  /*
   *  ImplementedInterfaceNames method takes an object and prints the name of the interfaces it implemented
   */
  static void ImplementedInterfaceNames(Object o){
    Class[] theInterfaces = o.getClass().getInterfaces();
    for (int i = 0; i < theInterfaces.length; i++){
      String interfaceName = theInterfaces[i].getName();
      System.out.println(interfaceName);
    }
  }

}
