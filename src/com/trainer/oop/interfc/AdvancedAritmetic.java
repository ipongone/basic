package com.trainer.oop.interfc;

public interface AdvancedAritmetic {
  // Implement this method with following detail:
  // divisorSum function just takes an integer as input and
  // return the sum of all its divisors.
  // For example divisors of 6 are 1, 2, 3 and 6, so divisor_sum should return 12.
  // The value of n will be at most 1000
  int divisor_sum(int n);

}
