package com.trainer.oop.inheritance;

public class Inheritance {
  // 1. Class bernama Arithmetic yang punya 1 method bernama add yang mengambil 2 parameter integer
  // dan return penjumlahan 2 parameter
  // 2. Class bernama Adder yang mewarisi Arithmetic
  //
  // Output:
  // My superclass is: Arithmetic
  // 42 13 20


  public static void main(String []args){
    // Create a new Adder object
    //Adder a = new Adder();

    // Print the name of the superclass on a new line
    //System.out.println("My superclass is: " + a.getClass().getSuperclass().getName());

    // Print the result of 3 calls to Adder's `add(int,int)` method as 3 space-separated integers:
    //System.out.print(a.add(10,32) + " " + a.add(10,3) + " " + a.add(10,10) + "\n");
  }

}
