package com.trainer.model;

public class ExamHistory {
  private String name;
  private String topic;
  private double score;

  public ExamHistory name(String name){
    this.name = name;
    return this;
  }
  public ExamHistory topic(String topic){
    this.topic = topic;
    return this;
  }
  public ExamHistory score(double score){
    this.score = score;
    return this;
  }
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }
  public String getTopic() {
    return topic;
  }
  public void setTopic(String topic) {
    this.topic = topic;
  }
  public double getScore() {
    return score;
  }
  public void setScore(double score) {
    this.score = score;
  }
}
